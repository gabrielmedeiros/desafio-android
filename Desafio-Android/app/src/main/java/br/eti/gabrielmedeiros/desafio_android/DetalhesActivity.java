package br.eti.gabrielmedeiros.desafio_android;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.gson.Gson;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import br.eti.gabrielmedeiros.desafio_android.R;
import br.eti.gabrielmedeiros.model.Endereco;

@EActivity(R.layout.activity_detalhes)
public class DetalhesActivity extends Activity {
    private Endereco endereco;

    @ViewById
    protected TextView txtCEP;

    @ViewById
    protected TextView txtTipoLogradouro;

    @ViewById
    protected TextView txtLogradouro;

    @ViewById
    protected TextView txtBairro;

    @ViewById
    protected TextView txtCidade;

    @ViewById
    protected TextView txtEstado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes);

        endereco = new Gson().fromJson(getIntent().getStringExtra("Endereco"), Endereco.class);

        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        txtCEP.setText(endereco.getCep());
        txtTipoLogradouro.setText(endereco.getTipoDeLogradouro());
        txtLogradouro.setText(endereco.getLogradouro());
        txtBairro.setText(endereco.getBairro());
        txtCidade.setText(endereco.getCidade());
        txtEstado.setText(endereco.getEstado());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}