package br.eti.gabrielmedeiros.util;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by gabrielmedeiros on 11/09/14.
 */
public class Util {
    public static boolean verificaConexaoInternet(Context context){
        boolean conectado = false;
        ConnectivityManager conectivtyManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conectivtyManager.getActiveNetworkInfo() != null &&
                conectivtyManager.getActiveNetworkInfo().isAvailable() &&
                conectivtyManager.getActiveNetworkInfo().isConnected()) {
            conectado = true;
        }
        return conectado;
    }
}
