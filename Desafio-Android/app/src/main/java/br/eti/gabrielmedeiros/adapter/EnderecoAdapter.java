package br.eti.gabrielmedeiros.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.eti.gabrielmedeiros.desafio_android.R;
import br.eti.gabrielmedeiros.model.Endereco;

/**
 * Created by gabrielmedeiros on 11/09/14.
 */
public class EnderecoAdapter extends BaseAdapter {
    private Context context;
    private List<Endereco> enderecos;

    public EnderecoAdapter(Context context, List<Endereco> enderecos){
        this.context = context;
        this.enderecos = enderecos;
    }

    @Override
    public int getCount() {
        return enderecos.size();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Endereco getItem(int i) {
        return enderecos.get(i);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Endereco endereco = enderecos.get(position);

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.listview_enderecos, null);

        TextView txtCEP = (TextView)view.findViewById(R.id.txtCEP);
        txtCEP.setText(endereco.getCep());

        return view;
    }
}
