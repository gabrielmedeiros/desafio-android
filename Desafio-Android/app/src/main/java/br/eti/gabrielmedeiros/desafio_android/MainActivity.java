package br.eti.gabrielmedeiros.desafio_android;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.EActivity;

import org.springframework.web.client.HttpClientErrorException;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

import br.eti.gabrielmedeiros.adapter.EnderecoAdapter;
import br.eti.gabrielmedeiros.model.Endereco;
import br.eti.gabrielmedeiros.util.Data;
import br.eti.gabrielmedeiros.util.Util;
import br.eti.gabrielmedeiros.webservice.WSClient;

@EActivity(R.layout.activity_main)
public class MainActivity extends Activity {
    @ViewById(R.id.edtCEP)
    protected EditText edtCEP;

    @RestService
    protected WSClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void consultar(View view){
        if (!Util.verificaConexaoInternet(this)){
            Toast.makeText(this, "Sem conexão", Toast.LENGTH_LONG).show();
            return;
        }

        if (edtCEP.getText().length() != 8){
            Toast.makeText(this, "Informe o CEP corretamente", Toast.LENGTH_LONG).show();
            return;
        }
        localizarEndereco();
    }

    @Background
    public void localizarEndereco() {
        try {
            Endereco endereco = client.procurarEndereco(edtCEP.getText().toString());
            salvarEndereco(endereco);
        } catch (HttpClientErrorException e) {
            mostrarMensagemError(e);
        }
    }

    @UiThread
    protected void mostrarMensagemError(HttpClientErrorException e){
        if (e.getStatusCode().value() == 404) {
            Toast.makeText(this, "CEP não encontrado", Toast.LENGTH_LONG).show();
        } else if (e.getStatusCode().value() == 400) {
            Toast.makeText(this, "Erro no servidor", Toast.LENGTH_LONG).show();
        }
    }

    @UiThread
    public void salvarEndereco(Endereco end){
        try {
            String endereco = new Gson().toJson(end);
            Data.salvar(endereco, getApplicationContext());

            Intent it = new Intent(this, DetalhesActivity_.class);
            it.putExtra("Endereco", endereco);
            startActivity(it);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Erro", Toast.LENGTH_LONG).show();
        }
    }

    public void historico(View view) {
        final List<Endereco> enderecos = new ArrayList<Endereco>();
        try {
            BufferedReader bufferedReader = Data.ler(getApplicationContext());

            String jsonEndereco;
            while ((jsonEndereco = bufferedReader.readLine()) != null) {
                Endereco endereco = new Gson().fromJson(jsonEndereco, Endereco.class);
                enderecos.add(endereco);
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Erro na exibição do histórico", Toast.LENGTH_LONG).show();
        }

        EnderecoAdapter adapter = new EnderecoAdapter(this, enderecos);

        ListView listView = new ListView(this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Endereco endereco = enderecos.get(i);
                Intent it = new Intent(MainActivity.this, DetalhesActivity_.class);
                it.putExtra("Endereco", new Gson().toJson(endereco));
                startActivity(it);
            }
        });

        Dialog dialog = new Dialog(this);
        dialog.setTitle("Desafio Android");
        dialog.setContentView(listView);
        dialog.show();
    }
}