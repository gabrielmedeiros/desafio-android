package br.eti.gabrielmedeiros.webservice;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.apache.http.HttpRequest;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

import java.io.IOException;

import br.eti.gabrielmedeiros.model.Endereco;

/**
 * Created by gabrielmedeiros on 08/09/14.
 */
@Rest(rootUrl = "http://correiosapi.apphb.com", converters = {GsonHttpMessageConverter.class})
public interface WSClient {
    @Get("/cep/{cep}")
    Endereco procurarEndereco(String cep);
}