package br.eti.gabrielmedeiros.util;

import android.content.Context;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Created by gabrielmedeiros on 08/09/14.
 */
public class Data {
    final static String ARQUIVO = "historico";

    public static void salvar(String value, Context context) throws Exception {
        FileOutputStream fileOutputStream = context.openFileOutput(ARQUIVO, Context.MODE_APPEND);
        OutputStreamWriter osw = new OutputStreamWriter(fileOutputStream);
        osw.write(value + "\n");
        osw.flush();
        fileOutputStream.close();
    }

    public static BufferedReader ler(Context context) throws Exception {
        FileInputStream fileInputStream = context.openFileInput(ARQUIVO);
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(fileInputStream));
        return inputReader;
    }
}
